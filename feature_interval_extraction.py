import numpy as np
from scipy.signal import argrelextrema

border_index = 25


def get_well_with_max_min_feature_interval(well, feature_name, border_changes_percent=0.15):
    intervals = get_interval_collector(well, feature_name, border_changes_percent)
    if len(intervals) == 0:
        print(well)
        well.plot(x='MD', y=feature_name, figsize=(10, 5), grid=True)
        well.plot(x='MD', y='Facies', figsize=(10, 5), grid=True)
        return well
    well_with_min = add_min_features(well, feature_name, intervals)
    # max_all = get_max_features(well_with_min, feature_name, intervals)
    # well_with_max = add_max_features(well_with_min, feature_name, intervals, max_all)
    return well_with_min


def get_interval_collector(well_copy, sp_or_gr, border_changes_percent):
    well = well_copy.copy()
    min_ = well[sp_or_gr].min()
    well[sp_or_gr] = well[sp_or_gr].apply(lambda x: x + abs(min_)) # необходимое условие для корректной работы

    sp_or_gr_min_all = get_min_all(well[sp_or_gr].values)
    sp_or_gr_max_all = get_max_all(well[sp_or_gr].values)

    intervals = []

    for min_index in sp_or_gr_min_all:
        nearest_max_index = get_nearest_max_for_value_index(min_index, sp_or_gr_max_all, well, sp_or_gr)
        value_min = well.iloc[min_index][sp_or_gr] - abs(min_)
        value_max = well.iloc[nearest_max_index][sp_or_gr] - abs(min_)
        if is_glina(index_min=min_index, index_max=nearest_max_index, value_min=value_min, value_max=value_max,
                    border_changes_percent=border_changes_percent):
            index_border = get_interval_border_index(min_index, nearest_max_index)
            depth_current = well.iloc[index_border]["MD"]
            distance_index = abs(nearest_max_index - min_index)
            distance_value = abs(well.iloc[nearest_max_index]["MD"] - well.iloc[min_index]["MD"])
            if value_min != value_max:
                distance_values_percent = 1 - abs(value_min) / abs(value_max)
            else:
                distance_values_percent = 0

            r = {'min_index': min_index,
                 'min_value': value_min,
                 'max_index_nearest': nearest_max_index,
                 'max_value_nearest': value_max,
                 'changes_values_percent': distance_values_percent,
                 'distance_border_index': distance_index,
                 'distance_border_value': distance_value,
                 'MD_min': well.iloc[min_index]["MD"],
                 'MD_max': well.iloc[nearest_max_index]["MD"],
                 }
            intervals.append(r)

    return intervals


def get_interval_border_index(min_index, nearest_max_index):
    if min_index < nearest_max_index:
        return min_index
    else:
        return nearest_max_index


def get_max_all(ar):
    maxIndWhere = np.zeros_like(ar)
    # print(maxIndWhere)
    peakVar = -np.inf
    i = -1
    while i < len(ar) - 1:
        # for i in range(len(ar)):
        i += 1
        if peakVar < ar[i]:
            peakVar = ar[i]
            for j in range(i, len(ar)):
                if peakVar < ar[j]:
                    break
                elif peakVar == ar[j]:
                    continue
                elif peakVar > ar[j]:
                    peakInd = i + np.floor(abs(i - j) / 2)
                    maxIndWhere[peakInd.astype(int)] = 1
                    i = j
                    break

        peakVar = ar[i]

    if peakVar == -np.inf:  # тут может возникнуть баг если максимумов нету, поэтому проверка
        return ar

    maxIndWhere = np.where(maxIndWhere)[0]
    # print(ar)
    # print(maxIndWhere)
    if len(maxIndWhere) == 0:
        return maxIndWhere
    last_index_max = maxIndWhere[-1]  # тут может возникнуть баг если максимумов нету
    is_exist_max_in_last_border = last_index_max < len(ar) and ar[last_index_max + 1] < ar[-1]
    if is_exist_max_in_last_border:
        maxIndWhere = np.append(maxIndWhere, len(ar) - 1)
    return maxIndWhere
    # return argrelextrema(values, np.greater)


def get_min_all(values):
    return get_max_all([-1 * x for x in values])
    # return argrelextrema(values, np.less)


def get_nearest_max_for_value_index_(index_min, array_max, well, sp_or_gr):
    def get_first_maximum_index(array_max, index_min_):
        minimum_distance = 1000000
        max_index = 0
        for index, max in enumerate(array_max):
            difference = abs(index_min_ - max)
            if difference <= minimum_distance:
                max_index = index
                minimum_distance = difference
            else:
                break
        return max_index

    if len(array_max) <= 1:
        return 0

    if index_min < array_max[0]:
        return array_max[0]

    max_index_first_in_array = get_first_maximum_index(array_max, index_min)
    max_index_value = array_max[max_index_first_in_array]


def get_nearest_max_for_value_index(index_min, array_max, well, sp_or_gr):
    def get_first_maximum_index(array_max, index_min_):
        minimum_distance = 1000000
        max_index = 0
        for index, max in enumerate(array_max):
            difference = abs(index_min_ - max)
            if difference <= minimum_distance:
                max_index = index
                minimum_distance = difference
            else:
                break
        return max_index

    def get_nearest_two_maximum_index(array_max, max_index_first_in_array):
        left_index = max_index_first_in_array - 1
        left = array_max[left_index]
        left_distance = array_max[max_index_first_in_array] - left
        right_index = max_index_first_in_array + 1
        right = array_max[right_index]
        right_distance = right - array_max[max_index_first_in_array]
        if abs(left_distance) < abs(right_distance):
            return left_index
        else:
            return right_index

    if len(array_max) <= 1:
        return 0

    if index_min < array_max[0]:
        return array_max[0]

    if index_min >= array_max[-1]:
        return array_max[-1]

    max_index_first_in_array = get_first_maximum_index(array_max, index_min)
    max_index_value = array_max[max_index_first_in_array]
    value_max_first = abs(well.iloc[max_index_value][sp_or_gr])

    if 0 < max_index_first_in_array < len(array_max) - 1:
        two_max_index = get_nearest_two_maximum_index(array_max, max_index_first_in_array)
        two_max_value = array_max[two_max_index]
        if value_max_first >= two_max_value:
            return max_index_value
        else:
            return two_max_value

    if max_index_first_in_array == 0:
        right = array_max[1]
        if value_max_first >= right:
            return max_index_value
        else:
            return right

    if max_index_first_in_array == len(array_max) - 1:
        left = array_max[-1]
        if value_max_first >= left:
            return max_index_value
        else:
            return left

    return max_index_value


def is_glina(index_min, index_max, value_min, value_max, border_changes_percent):
    distance_index = abs(index_max - index_min)
    distance_values_percent = abs(value_min) / abs(value_max)
    return distance_index < border_index and 1 - distance_values_percent >= border_changes_percent


def add_min_features(well, name_feature, intervals):
    well_copy = well.copy()
    well_copy[name_feature + '_interval_min'] = well_copy.iloc[0][name_feature]

    current_min = well_copy.iloc[0][name_feature]
    for index, row in well_copy.iterrows():
        current_depth = row['MD']
        if current_depth in intervals:
            current_min = row[name_feature]

        well_copy.loc[index, name_feature + '_interval_min'] = current_min
    return well_copy


def get_max_features(well, name_feature, intervals):
    def get_maximums(depth_start, depth_finich):
        polu_interval_df = well[well['MD'] <= depth_finich]
        interval_df = polu_interval_df[polu_interval_df['MD'] > depth_start]
        max_feature_interval = interval_df[name_feature].max()
        return max_feature_interval

    max_all = []
    for index, interval in enumerate(intervals):
        if len(intervals) == 1:
            md_start = well.iloc[0]["MD"]
            m = get_maximums(md_start, interval)
            max_all.append(m)
            md_end = well.tail(1).iloc[0]["MD"]
            m = get_maximums(interval, md_end)
            max_all.append(m)
            continue

        if index == 0:
            md_start = well.iloc[0]["MD"]
            m = get_maximums(md_start, interval)
            max_all.append(m)
            continue

        m_ = get_maximums(intervals[index - 1], interval)
        max_all.append(m_)

        if index == len(intervals) - 1:
            md_end = well.tail(1).iloc[0]["MD"]
            m = get_maximums(interval, md_end)
            max_all.append(m)
            continue

    return max_all


def add_max_features(well, name_feature, intervals, max_all):
    well_copy = well.copy()
    well_copy[name_feature + '_interval_max'] = -999

    if len(max_all) == 0:
        return well_copy
    index_max = 0
    current_max = max_all[index_max]
    for index, row in well_copy.iterrows():
        current_depth = row['MD']
        if current_depth in intervals:
            index_max = index_max + 1
            current_max = max_all[index_max]

        well_copy.loc[index, name_feature + '_interval_max'] = current_max
    return well_copy


def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    import numpy as np
    from math import factorial

    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError:
        raise ValueError("window_size and order have to be of type int")
        if window_size % 2 != 1 or window_size < 1:
            raise TypeError("window_size size must be a positive odd number")
        if window_size < order + 2:
            raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order + 1)
    half_window = (window_size - 1) // 2
    # precompute coefficients
    b = np.mat([[k ** i for i in order_range] for k in range(-half_window, half_window + 1)])
    m = np.linalg.pinv(b).A[deriv] * rate ** deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs(y[1:half_window + 1][::-1] - y[0])
    lastvals = y[-1] + np.abs(y[-half_window - 1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve(m[::-1], y, mode='valid')

# На псевдокоде вероятно это будет выглядеть следующим образом
#
# исходные данные:
# скважина [[глубина, значение_SP], [глубина, значение_SP] ... , [глубина, значение_SP]]
#
#
# SP_минимумы = получить_минимумы_SP(скважина)
# SP_максимумы = получить_максимумы_SP(скважина)
#
# интервалы = []
#
# пройтись по всем SP_минимумы
#       SP_максимум_ближайший = найти ближайший максимум до текущий глубины (SP_минимум_текущий)
#       если это_глина_песчаник(SP_минимум_текущий, SP_максимум_ближайший)
#                интервалы.добавить(текущию_глубину)
#
#
#
# это_глина_песчаник(SP_минимум_текущий, SP_максимум_ближайший)
#       глубина_мин_sp <- SP_минимум_текущий
#       глубина_мах_sp <- SP_максимум_ближайший
#       если глубина_мин_sp - глубина_мах_sp < 1 метр и (глубина_мах_sp - глубина_мин_sp) > порог
#                   вернуть да
#       вернуть нет
#
# на выходе будет например
#
# интервалы [2300, 2500, 2550, 2380]
#
# получается интервал
# начало известных значений sp - 2300
# 2300-2500
# 2500-2550
# 2550-2380
# 2380-окончание известных значений sp
#
# по этим интервалам дальше уже считать ASP, AGR


