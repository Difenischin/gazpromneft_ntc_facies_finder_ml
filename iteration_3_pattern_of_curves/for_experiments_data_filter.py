# Загружаем необходимые библиотеки
import pandas as pd
import numpy as np
import random as rd
import seaborn as sns
import lasio
from os import listdir
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import f1_score, accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV, cross_val_predict, cross_val_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import mean_squared_error
from IPython.display import display, clear_output
from sklearn.model_selection import StratifiedKFold
import matplotlib.pyplot as plt
import talib, math


# работаем по gr
#
# удаляем незначимые символы
#
# удалаем линейный тренд
#
# проверяем на страцианарность
# аналогично делаем по тренировочному и проверочному набору данных
# выявляем паттерны
# проверяем их взаимной корреляцией
# добавить скользяшую среднию на нужном диапазоне
# @Aleksandra_Pandina


def get_for_learn_clean_df(statistic_df):
    feature = statistic_df.copy()
    del feature['datasetName']
    feature = feature[feature["MD"] * 10 % 2 < 0.001]
    feature = feature[feature["MD"] > 2000]
    return feature


def get_for_learn_clean_df_spk1799PL(statistic_df):
    feature = statistic_df.copy()
    del feature['datasetName']
    feature = feature[feature['wellName'] == 'spk1799PL']
    feature = feature[feature["MD"] > 2000]
    feature = feature[feature["MD"] * 10 % 2 > 0.001]
    feature['wellName'] = 'spk1799PL_shift'
    return feature


def get_well_dfs(all_well_df):
    wall_names_ = all_well_df['wellName'].unique()
    print(wall_names_)
    wells = []
    for name in wall_names_:
        well = all_well_df[all_well_df['wellName'] == name]
        wells.append(well)
    return wells


def test_stationarity(timeseries, window, name_feature):
    # window = 2  - 1 сдвиг это 0.2 м - теоритически обоснованное значение 0.4 м
    from statsmodels.tsa.stattools import adfuller
    # Determing rolling statistics
    rolmean = timeseries.rolling(window=window, center=True).mean()
    rolstd = timeseries.rolling(window=window, center=True).std()

    # Plot rolling statistics:
    plt.figure(figsize=(10, 5))

    plt.plot(timeseries, color='blue', label='Original')
    plt.plot(rolmean, color='red', label='Rolling Mean')
    plt.plot(rolstd, color='black', label='Rolling Std')

    plt.legend(loc='best')
    plt.title('Rolling Mean & Standard Deviation')
    plt.xlabel('MD')
    plt.ylabel(name_feature)
    plt.show(block=False)

    # Perform Dickey-Fuller test:
    print('Results of Dickey-Fuller Test:')
    dftest = adfuller(timeseries, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], index=['Test Statistic', 'p-value', '#Lags Used', 'Number of Observations Used'])
    for key, value in dftest[4].items():
        dfoutput['Critical Value (%s)' % key] = int(value)
    display(dfoutput)


def get_preprocessing(df_in, feature_name, undefined_value=-9999):
    df = df_in.copy()
    df[feature_name] = df[feature_name].fillna(undefined_value)
    df[feature_name] = df[feature_name].apply(lambda x: round(x, 1))
    print(df[feature_name])
    wells = []
    for name in df['wellName'].unique():
        well = df[df['wellName'] == name]
        well[feature_name] = well[feature_name] - well[feature_name].shift(1)
        wells.append(well)
    all_wells = pd.concat(wells)
    all_wells[feature_name] = all_wells[feature_name].fillna(0)
    all_wells[feature_name] = all_wells[feature_name].apply(lambda x: x if -999 < x < 999 else 0)
    print(all_wells[feature_name])
    return all_wells


def get_new_sintetic_well(well, feature_name, count_copy):
    np.random.seed(20)
    mu, sigma = 0, 0.5
    sintetic_wells = []
    for i in range(count_copy):
        well_copy = well.copy()
        well_copy['wellName'] = well_copy['wellName'] + '_random_noise_' + str(i)
        noise = np.random.normal(mu, sigma, [len(well[feature_name])])
        well_copy[feature_name] = well_copy[feature_name] + noise
        sintetic_wells.append(well_copy)
    return sintetic_wells


for_ml = get_for_learn_clean_df(pd.read_csv('Correct_train_26.csv', sep=';'))
spk1799PL = get_for_learn_clean_df_spk1799PL(pd.read_csv('Correct_train_26.csv', sep=';'))
for_preproc = pd.concat([for_ml, spk1799PL])

GR = 'GR'

test_stationarity(timeseries=for_preproc[GR], window=2, name_feature=GR)

for_preproc = pd.concat(get_new_sintetic_well(for_preproc, GR, 2))

for_ml = get_preprocessing(for_preproc, GR)
for_ml[GR + '_default'] = for_preproc[GR]
test_stationarity(timeseries=for_ml[GR], window=2, name_feature=GR)


def get_bets_intervals_starts(wells, feature_name):
    def get_part_feature(well, md_start, depth_delta):
        part = well[well['MD'] >= md_start]
        part = part[part['MD'] < md_start + depth_delta]
        return part[feature_name]

    result = pd.DataFrame()
    depth_delta = 6

    for well in wells:
        copy = well.copy()
        if 'spk1519' in well['wellName'].unique():
            v = get_part_feature(copy, 2866.5, depth_delta)
            result['spk1519'] = v.values
            continue
        #
        # if 'spk1521' in well['wellName'].unique():
        #     v = get_part_feature(copy, 3026, depth_delta)
        #     result['spk1521'] = v.values
        #     continue

        # if 'spk1527'  in well['wellName'].unique(): лучше без неё
        #     print(well)
        #     copy = copy[copy['MD'] >= 3680]
        #     copy = copy[copy['MD'] < 3680 + depth_delta]
        #     return_wells.append(copy)
        #     continue

        # if 'spk1749PL' in well['wellName'].unique():
        #     v = get_part_feature(copy, 2623, depth_delta)
        #     array_ = v.values
        #     array_[0] = array_[1]
        #     result['spk1749PL'] = v.values
        #
        #     continue
        # # скважина нормальная, просто из за сдвига теряются данные
        # if 'spk429L' in well['wellName'].unique():
        #     v = get_part_feature(copy, 2545.4, depth_delta)
        #     result['spk429L'] = v.values
        #     continue

    return result


parts_7_facies_df = get_bets_intervals_starts(get_well_dfs(for_ml), GR)

display(parts_7_facies_df)
display(parts_7_facies_df.corr())


def get_correlate_intervals(wells_all, best_signal, feature_name):
    import numpy
    wells = get_well_dfs(wells_all)
    for well in wells:
        correlations_part = numpy.correlate(well[feature_name].values, best_signal.values, mode="same")
        well['correlations_part'] = correlations_part
    result_df = pd.concat(wells)
    return result_df['correlations_part']


def get_with_correlate(wells_all, signals_7_facies, feature_name):
    name_wells = signals_7_facies.columns
    result = wells_all.copy()
    for name_well in name_wells:
        result[name_well + '_corr'] = get_correlate_intervals(wells_all,
                                                              signals_7_facies[name_well],
                                                              feature_name)
        result[name_well + '_corr_sum'] = result[name_well + '_corr'].rolling(len(signals_7_facies[name_well])).sum()
        result[name_well + '_probability'] = result[name_well + '_corr_sum'] / result[name_well + '_corr_sum'].max()
    return result


predicts_df = get_with_correlate(for_ml, parts_7_facies_df, GR)


def predict_facies_stationary(row):
    f_0 = row['spk1519_corr_sum']
    if f_0 > 2000:
        return 7
    return 3


global_name_predict_model = 'predict_spk1519corr_sum'
predicts_df[global_name_predict_model] = predicts_df.apply(predict_facies_stationary, axis=1)
predicts_df = predicts_df.rename(columns={"GR_default": "GR", "GR": "GR_detrend"})
display(predicts_df.describe())
display(predicts_df)


# predicts_df['predict_spk1519corr_sum'] = predicts_df.apply(predict_facies_stationary, axis=1)
def get_wells_with_7_facies(wells_all):
    wells_7_namens = wells_all[wells_all[global_name_predict_model] == 7]['wellName'].unique()
    print(wells_7_namens)
    wells = []
    for name in wells_7_namens:
        well = wells_all[wells_all['wellName'] == name]
        wells.append(well)
    return pd.concat(wells)


predicts_7_fecies_df = get_wells_with_7_facies(predicts_df)
predicts_7_fecies_df.to_csv('train_7_predict_for_spk1519.csv', sep=';')

get_new_sintetic_well(predicts_df, GR, 2)
