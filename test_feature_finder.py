import unittest
import pandas as pd

from feature_interval_extraction import add_min_features, add_max_features, get_max_features, get_interval_collector, \
    get_max_all, get_min_all, get_nearest_max_for_value_index, is_glina, get_well_with_max_min_feature_interval, \
    savitzky_golay


class MyTestCase(unittest.TestCase):

    def test_nearest_max_index(self):
        max_index = [3, 7, 10, 16]
        df = self.get_base_df()
        self.assertEqual(3, get_nearest_max_for_value_index(1, max_index, df, "SP"))
        self.assertEqual(3, get_nearest_max_for_value_index(2, max_index, df, "SP"))
        self.assertEqual(7, get_nearest_max_for_value_index(4, max_index, df, "SP"))
        self.assertEqual(10, get_nearest_max_for_value_index(8, max_index, df, "SP"))
        self.assertEqual(16, get_nearest_max_for_value_index(14, max_index, df, "SP"))

    def test_is_glina(self):
        self.assertEqual(True, is_glina(1, 2, 5, 10, 0.01))
        # self.assertEqual(False, is_glina(1, 8, 5, 10, 0.3)) #потамушто индекс дистанции вшит

    def test_find_min_df(self):
        df = self.get_base_df()
        lical_min_sp_values = get_min_all(df["SP"].values)
        self.assertEqual(lical_min_sp_values[0], 1)
        self.assertEqual(lical_min_sp_values[1], 4)
        self.assertEqual(lical_min_sp_values[2], 8)
        self.assertEqual(lical_min_sp_values[3], 11)

    def get_base_df(self):
        return pd.DataFrame(
            columns=["MD", "SP", "GR"],
            data=[[2011.2, 1, 13.6],  # 0
                  [2011.4, 1, 13.6],  # 1
                  [2011.6, 2, 13.6],  # 2
                  [2011.8, 3, 13.6],  # 4
                  [2012, 1, 13.6],
                  [2012.2, 5, 13.6],
                  [2012.4, 6, 13.6],
                  [2012.6, 7, 13.6],
                  [2012.8, 6, 13.6],
                  [2013, 7, 13.6],
                  [2013.2, 8, 13.6],
                  [2013.4, 2, 13.6],
                  [2013.6, 3, 13.6],
                  [2013.8, 4, 13.6],
                  [2014.2, 5, 13.6],
                  [2014.4, 6, 13.6],
                  [2014.6, 8, 13.6],
                  ])

    def test_find_max_df(self):
        df = self.get_base_df()
        lical_min_sp_values = get_max_all(df["SP"].values)
        self.assertEqual(lical_min_sp_values[0], 3)
        self.assertEqual(lical_min_sp_values[1], 7)
        self.assertEqual(lical_min_sp_values[2], 10)

    def test_get_interval(self):
        df = self.get_base_df()
        intervals = get_interval_collector(df, 'SP', 0.2)
        interval_result = [{'min_index': 1, 'min_value': 1.0, 'max_index_nearest': 3, 'max_value_nearest': 3.0,
                            'changes_values_percent': 0.6666666666666666, 'distance_border_index': 2,
                            'distance_border_value': 0.3999999999998636, 'MD_min': 2011.4, 'MD_max': 2011.8},
                           {'min_index': 4, 'min_value': 1.0, 'max_index_nearest': 7, 'max_value_nearest': 7.0,
                            'changes_values_percent': 0.8571428571428571, 'distance_border_index': 3,
                            'distance_border_value': 0.599999999999909, 'MD_min': 2012.0, 'MD_max': 2012.6},
                           {'min_index': 8, 'min_value': 6.0, 'max_index_nearest': 10, 'max_value_nearest': 8.0,
                            'changes_values_percent': 0.25, 'distance_border_index': 2,
                            'distance_border_value': 0.40000000000009095, 'MD_min': 2012.8, 'MD_max': 2013.2},
                           {'min_index': 11, 'min_value': 2.0, 'max_index_nearest': 10, 'max_value_nearest': 8.0,
                            'changes_values_percent': 0.75, 'distance_border_index': 1,
                            'distance_border_value': 0.20000000000004547, 'MD_min': 2013.4, 'MD_max': 2013.2}]
        self.assertEqual(intervals[0], interval_result[0])

    def test_get_interval_df(self):
        def get_df_interval_result(well, intervals):
            dfs_parts = []
            for interval in intervals:
                min_ = interval['MD_min']
                max_ = interval['MD_max']
                if min_ < max_:
                    df_part_min = well[well['MD'] >= min_]
                    df_part = df_part_min[df_part_min['MD'] <= max_]
                else:
                    df_part_min = well[well['MD'] <= min_]
                    df_part = df_part_min[df_part_min['MD'] >= max_]
                dfs_parts.append(df_part)
            all = pd.concat(dfs_parts)
            return all.drop_duplicates()

        def get_df_intervals_result(well, intervals):
            dfs_parts = []
            for interval in intervals:
                min_ = interval['MD_min']
                max_ = interval['MD_max']
                if min_ < max_:
                    df_part_min = well[well['MD'] >= min_]
                    df_part = df_part_min[df_part_min['MD'] <= max_]
                else:
                    df_part_min = well[well['MD'] <= min_]
                    df_part = df_part_min[df_part_min['MD'] >= max_]
                dfs_parts.append(df_part)
            return dfs_parts

        def get_df_middle_intervals_result(well, intervals):
            def get_left_md(interval):
                min_ = interval['MD_min']
                max_ = interval['MD_max']
                if min_ < max_:
                    return min_
                else:
                    return max_

            def get_right_md(interval):
                min_ = interval['MD_min']
                max_ = interval['MD_max']
                if min_ < max_:
                    return max_
                else:
                    return min_

            if len(intervals) == 0:
                return 0
            well_copy = well.copy()
            if len(intervals) == 1:
                left = get_left_md(intervals[0])
                right = get_right_md(intervals[0])
                left_df = well_copy[well_copy['MD'] < left]
                right_df = well_copy[well_copy['MD'] > right]
                return [left_df, right_df]

            middle_interval_dfs = []
            for index, interval in enumerate(intervals):
                if index == 0:
                    left = get_left_md(intervals[index])
                    left_df = well_copy[well_copy['MD'] < left]
                    middle_interval_dfs.append(left_df)
                    continue
                if index == len(intervals) - 1:
                    right = get_right_md(intervals[index])
                    right_df = well_copy[well_copy['MD'] > right]
                    middle_interval_dfs.append(right_df)
                    continue
                right_current = get_right_md(intervals[index])
                left_next = get_left_md(intervals[index + 1])
                df_part_right = well_copy[well_copy['MD'] > right_current]
                df_part = df_part_right[df_part_right['MD'] < left_next]
                if not df_part.empty:
                    middle_interval_dfs.append(df_part)
            return middle_interval_dfs

        def add_statistic_for_interval(middle_interval_dfs, feature_names):
            results = []
            for interval_df_ in middle_interval_dfs:
                part = interval_df_.copy()
                for name in feature_names:
                    part['max_' + name] = part[name].max()
                    part['min_' + name] = part[name].min()
                    if part[name].max() == part[name].min():
                        part['std_' + name] = 0
                    else:
                        part['std_' + name] = part[name].std()
                    part['mean_' + name] = part[name].mean()
                results.append(part)
            return pd.concat(results)

        def get_with_interval_feature_extraction(well, features_name_for_extraction,
                                                 percent_change_border, features_name_for_statistic=['SP', 'GR']):
            intervals = get_interval_collector(well, features_name_for_extraction, percent_change_border)
            interval_df = get_df_intervals_result(well, intervals)
            middle_interval_df = get_df_middle_intervals_result(well, intervals)
            with_statistic_middle = add_statistic_for_interval(middle_interval_df, features_name_for_statistic)
            with_statistic_intervals = add_statistic_for_interval(interval_df, features_name_for_statistic)
            concat_df = pd.concat([with_statistic_middle, with_statistic_intervals])
            remove_duplicates = concat_df.drop_duplicates(subset='MD', keep="last")
            return remove_duplicates.sort_index()

        df = self.get_base_df()
        interval_result = [{'min_index': 1, 'min_value': 1.0, 'max_index_nearest': 3, 'max_value_nearest': 3.0,
                            'changes_values_percent': 0.6666666666666666, 'distance_border_index': 2,
                            'distance_border_value': 0.3999999999998636, 'MD_min': 2011.4, 'MD_max': 2011.8},
                           {'min_index': 4, 'min_value': 1.0, 'max_index_nearest': 7, 'max_value_nearest': 7.0,
                            'changes_values_percent': 0.8571428571428571, 'distance_border_index': 3,
                            'distance_border_value': 0.599999999999909, 'MD_min': 2012.0, 'MD_max': 2012.6},
                           {'min_index': 8, 'min_value': 6.0, 'max_index_nearest': 10, 'max_value_nearest': 8.0,
                            'changes_values_percent': 0.25, 'distance_border_index': 2,
                            'distance_border_value': 0.40000000000009095, 'MD_min': 2012.8, 'MD_max': 2013.2},
                           {'min_index': 11, 'min_value': 2.0, 'max_index_nearest': 10, 'max_value_nearest': 8.0,
                            'changes_values_percent': 0.75, 'distance_border_index': 1,
                            'distance_border_value': 0.20000000000004547, 'MD_min': 2013.4, 'MD_max': 2013.2}]
        interval_df = get_df_intervals_result(df, interval_result)
        df_with_features = get_with_interval_feature_extraction(df, 'SP', 0.2, ['SP', 'GR'])
        # middle_interval_df = get_df_middle_intervals_result(df, interval_result)
        # with_statistic_middle = add_statistic_for_interval(middle_interval_df, ['SP'])
        # with_statistic_intervals = add_statistic_for_interval(interval_df, ['SP'])
        # with_statistic_intervals = add_statistic_for_interval(interval_df, ['SP'])
        self.assertEqual(interval_df, interval_result[0])

    def test_add_min_features(self):
        df = self.get_base_df()
        intervals = get_interval_collector(df, 'SP', 0.15)
        # df_with_features = add_min_features(df, 'SP', intervals)
        # self.assertEqual(df_with_features.iloc[0]['SP_interval_min'], 1)
        # self.assertEqual(df_with_features.iloc[15]['SP_interval_min'], 2)

    def test_get_max_features(self):
        df = self.get_base_df()
        intervals = get_interval_collector(df, 'SP', 0.15)
        # max_all = get_max_features(df, 'SP', intervals)
        # self.assertEqual(max_all[0], 3)
        # self.assertEqual(max_all[1], 8)
        # self.assertEqual(max_all[2], 8)

    def test_add_max_features(self):
        df = self.get_base_df()
        intervals = get_interval_collector(df, 'SP', 0.15)
        # max_all = get_max_features(df, 'SP', intervals)
        # df_with_max = add_max_features(df, 'SP', intervals, max_all)
        # self.assertEqual(df_with_max.iloc[0]['SP_interval_max'], 3)
        # self.assertEqual(df_with_max.iloc[15]['SP_interval_max'], 8)

    def test_get_well_with_max_min_feature_interval(self):
        df = self.get_base_df()
        df_with_feature = get_well_with_max_min_feature_interval(df, 'SP')
        self.assertEqual(df_with_feature.iloc[0]['SP_interval_min'], 1)
        self.assertEqual(df_with_feature.iloc[15]['SP_interval_min'], 2)
        self.assertEqual(df_with_feature.iloc[0]['SP_interval_max'], 3)
        self.assertEqual(df_with_feature.iloc[15]['SP_interval_max'], 8)

    def test_gget_well_with_max_min_feature_interval_(self):
        import pandas as pd
        import numpy as np
        from IPython.display import display

        def get_well_dfs(all_well_df):
            wall_names_ = all_well_df['wellName'].unique()
            wells = []
            for name in wall_names_:
                well = all_well_df[all_well_df['wellName'] == name]
                wells.append(well)
            return wells

        def add_wells_features(wells, features_for_detrend):
            wells_new = []
            for well in wells:
                wells_new.append(detrend(well, features_for_detrend))
            return wells_new

        def detrend(df, features_for_detrend):
            undefined_value = -9999

            def check_correct_md(df_):
                def is_md_correct(row, md, md_last):
                    difference = row[md_last] - row[md]
                    if (difference - 0.2) > 0.001:
                        display(row)
                    return row

                copy_df_ = df_.copy()
                copy_df_['MD_last'] = copy_df_['MD'].shift(-1)
                copy_df_.apply(
                    lambda row: is_md_correct(row, 'MD', 'MD_last'), axis=1)
                del copy_df_['MD_last']

            def corrector_detrend(row, name_base, name_detrend):
                if row[name_base] < -999:
                    return undefined_value
                if row[name_base] > 500:
                    return undefined_value
                if row[name_detrend] > 999:
                    return undefined_value
                if row[name_detrend] < -999:
                    return undefined_value
                return row[name_detrend]

            import math
            def corrector_nulls(value):
                if math.isnan(value):
                    return undefined_value
                if math.isinf(value):
                    return undefined_value
                if value == 'NaN':
                    return undefined_value
                return value

            def remove_trend(serials):
                from scipy import signal
                return signal.detrend(serials)

            check_correct_md(df.copy())
            copy_df = df.copy()

            for feature in features_for_detrend:
                prefix = '_det'
                copy_df[feature + prefix] = copy_df[feature] - copy_df[feature].shift(-1)
                copy_df[feature] = copy_df[feature].apply(corrector_nulls)
                copy_df[feature + prefix] = remove_trend(copy_df[feature])
                copy_df[feature + '_detrend'] = copy_df.apply(
                    lambda row: corrector_detrend(row, feature, feature + prefix), axis=1)
                del copy_df[feature + prefix]
            return copy_df

        def get_for_learn_clean_df(statistic_df):
            feature = statistic_df.copy()
            # del feature['datasetName']
            # feature = feature[feature["Facies"] >= -999]
            feature = feature[feature["GR"] >= -999]
            feature = feature[feature["SP"] >= -999]
            feature = feature[feature["MD"] * 10 % 2 < 0.001]
            feature["SP_"] = savitzky_golay(feature["SP"].values, 32, 4)
            # feature["SP_"] = feature["SP_"].apply(np.int64)
            return feature

        name_dataset = 'DATATRAIN18+LAYER.csv'  # 'DATATRAINSHORTSTANDLAY.csv'
        wells_base_df = get_for_learn_clean_df(pd.read_csv(name_dataset, sep=';'))
        display(wells_base_df.describe())
        wells_samples_df = get_well_dfs(wells_base_df)

        # wells_add_detrends_df = add_wells_features(wells_samples_df, [])

        def get_well_with_distance_features(well):
            from feature_interval_extraction import get_well_with_max_min_feature_interval
            well_copy = get_well_with_max_min_feature_interval(well, "SP_")
            return well_copy

        def get_well_with_features_df(wells):
            wells_return = []
            wells_nulls = []
            for well in wells:
                well_with_new_features = get_well_with_distance_features(well)
                if 'SP__interval_min' in well_with_new_features.columns:
                    wells_return.append(well_with_new_features)
                else:
                    wells_nulls.append(well_with_new_features)
            return wells_return

        # well_with_features = get_well_with_features_df(wells_samples_df)
        # for_ml_preprocessing = pd.concat(well_with_features)
    #     # df = self.get_base_df()
    #     # df_with_feature = get_well_with_max_min_feature_interval(df, 'SP_')
    #     # self.assertEqual(df_with_feature.iloc[0]['SP_interval_min'], 1)
    #     # self.assertEqual(df_with_feature.iloc[15]['SP_interval_min'], 2)
    #     # self.assertEqual(df_with_feature.iloc[0]['SP_interval_max'], 3)
    #     # self.assertEqual(df_with_feature.iloc[15]['SP_interval_max'], 8)


# def test_something(self):
#     input = np.array([1, 1, 2, 3, 1, 5, 6, 7, 6, 7, 8, 2, 3, 4, 5, 6, 8])
#     # resul = np.array([1, 1, 2, 3, 1, 5, 6, 7, 6, 7, 8, 2, 2, 4, 5, 6, 8])
#     min_index = np.array([4, 8, 11])
#     max_index = np.array([3, 7, 10])
#     self.assertEqual(lenmin_index, argrelextrema(input, np.less))
#     self.assertEqual(max_index, argrelextrema(input, np.greater))
#     result_glina_index = [4, 11]
#     result_min = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2]
#     # result_min = [1, 1, 2, 3, 1, 5, 6, 7, 6, 7, 8, 2, 2, 4, 5, 6, 8]
#     result_max = [1, 1, 3, 3, 1, 7, 7, 7, 6, 8, 8, 2, 2, 8, 8, 8, 8]
#
#     lical_max = argrelextrema(input, np.greater)
#     lical_min = argrelextrema(input, np.less)
#
#     peaks, _ = find_peaks(input, height=6)
#     plt.plot(input)
#     plt.plot(peaks, input[peaks], "x")
#     plt.plot(np.zeros_like(input), "--", color="gray")
#     plt.show()
#
#     peaks_distance, _ = find_peaks(input, distance=4)
#     # difference between peaks is >= 150
#     print(np.diff(peaks_distance))
#     # prints [186 180 177 171 177 169 167 164 158 162 172]
#
#     plt.plot(input)
#     plt.plot(peaks_distance, input[peaks_distance], "x")
#     plt.show()
#     self.assertEqual(True, False)

if __name__ == '__main__':
    unittest.main()


# def get_well_with_distance_features(well, feature_names):
#     well_copy = well.copy()
#     for name in feature_names:
#         # well_copy[name + '_savitzky_golay1'] = savitzky_golay(well_copy[name].values, 55, 7, 1)
#         # well_copy[name + '_savitzky_golay2'] = savitzky_golay(well_copy[name].values, 15, 7, 1)
#         well_copy[name + '_savitzky_golay3'] = savitzky_golay(well_copy[name].values, 55, 7, 1)
#         # well_copy = get_well_with_max_min_feature_interval(well_copy, name + '_savitzky_golay', 0.2)
#         # well_copy[name + '_savitzky_golay_two'] = well_copy[name + '_savitzky_golay']
#         # well_copy = get_well_with_max_min_feature_interval(well_copy, name + '_savitzky_golay_two', 0.2)
#         # well_copy[name + '_int'] = well_copy[name].apply(np.int64)
#         # well_copy = get_well_with_max_min_feature_interval(well_copy, name + '_int', 0.18)
#         # well_copy[name + '_int'] = well_copy[name].apply(np.int64)
#         # well_copy[name + '_savitzky_golay'] = savitzky_golay(well_copy[name + '_int'].values, 51, 9)
#         # well_copy[name + '_savitzky_golay_int'] = well_copy[name + '_savitzky_golay'].apply(np.int64)
#         # well_copy = get_well_with_max_min_feature_interval(well_copy, name + '_savitzky_golay_int', 0.18)
#         # well_copy = get_well_with_distance_features_(well_copy)
#
#     return well_copy


def add_interval_column(middle_interval_dfs, new_column_name, feature_sp_gr):
    results = []
    for interval_df_ in middle_interval_dfs:
        part = interval_df_.copy()
        part['max_' + new_column_name] = part[feature_sp_gr].max()
        part['min_' + new_column_name] = part[feature_sp_gr].min()
        results.append(part)
    return pd.concat(results)


def add_interval_column_nulls(middle_interval_dfs, new_column_name):
    results = []
    for interval_df_ in middle_interval_dfs:
        part = interval_df_.copy()
        part['max_' + new_column_name] = -9999
        part['min_' + new_column_name] = -9999
        results.append(part)
    return pd.concat(results)


def get_column_with_interval(well, features_name_for_extraction,
                             percent_change_border, new_column_name, feature_sp_or_gr):
    intervals = get_interval_collector(well, features_name_for_extraction, percent_change_border)
    interval_df = get_df_intervals_result(well, intervals)
    middle_interval_df = get_df_middle_intervals_result(well, intervals)
    with_statistic_middle = add_interval_column_nulls(middle_interval_df, new_column_name)
    with_statistic_intervals = add_interval_column(interval_df, new_column_name, feature_sp_or_gr)
    concat_df = pd.concat([with_statistic_middle, with_statistic_intervals])
    remove_duplicates = concat_df.drop_duplicates(subset='MD', keep="last")
    return remove_duplicates.sort_index()
